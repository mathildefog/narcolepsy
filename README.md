# Search for antigen specific clonotypes in donor's repertoires (Java code)  #

Copyright (C) 2018  Mathilde Foglierini Perez

email: mathilde.perez@irb.usi.ch

### SUMMARY ###

We have made available here Java source code to search for antigen specific clonotype (TCR) in some donors' repertoire generated by ImmunoSEQ Analyzer.

The scripts are primarily intended as reference for manuscript "T cells in patients with narcolepsy target self-antigens of hypocretin neurons, Nature, Latorre et al." (https://doi.org/10.1038/s41586-018-0540-1) rather than a stand-alone application.



### LICENSES ###

This code is distributed open source under the terms of the GNU Free Documentation License.


### INSTALL ###


Java JDK 8 https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html#A1097144


### PIPELINE ###

The main class 'FindAgSpecificTcrs' is used to search for antigen specific clonotypes in all donors' repertoires.

It needs 2 input files in a folder (see 'input' folder for examples):

  * AgSpecificClonotypes.txt, a table containing the antigen specific clonotypes annotated by IMGT
  * RearrangementDetails.tsv, a table regrouping all clonotypes from all donors' repertoire, generated automatically
    by ImmunoSEQ Analyzer from the Rearrangement View -> Export view to file (default parameters)
	
 It will create an output file 'AgSpecificClonotypes_found.tsv' that lists all antigen specific clonotypes and the related
  sample(s) and donor(s) it was found in.

 Of note: a clonotype is defined as a unique combination of a CDR3 amino acid sequence and its related V gene.


package ch.irb.narcolepsy;

import java.util.HashMap;

/**
 Copyright 2018 - Mathilde Foglierini Perez
 This code is distributed open source under the terms of the GNU Free Documentation License.

 This class implements a Sample object, where the donor, and subtype of the sample is stored
 */

public class ImplementSamples {
    private HashMap<String,Sample> sampleIdToSample = new HashMap<>();


    public ImplementSamples(String category){ //can be narco, ms or hd
        if (category.equals("narco")) {
            implementNarco();
        }
        else if (category.equals("ms")){
            implementMs();
        }
        else if (category.equals("hd")){
            implementHd();
        }
        else if (category.equals("all")){
            implementNarco();
            implementMs();
            implementHd();
            implementHd6();
        }
    }

    private void implementNarco(){
        //cd4 mem
        setSample("K02_CD4_M_Blood","narco","Pt9","CD4M",true);
        setSample("K08_CD4_M_Blood","narco","Pt8","CD4M",true);
        setSample("K09_CD4_M_Blood","narco","Pt4","CD4M",true);
        setSample("NCL6_CD4_M_Blood","narco","Pt3","CD4M",true);
        setSample("NCL7_cd4_mem_blood","narco","Pt10","CD4M",true);
        setSample("NarcoBA_CD4+_tot_PBMCs","narco","Pt1","CD4M",true);
        setSample("NarcoTCE_CD4+_tot_PBMCs","narco","Pt2","CD4M",true);
        setSample("Pt11_K10_CD4M_Blood","narco","Pt11","CD4M",true);
        setSample("Pt12_K11_CD4M_Blood","narco","Pt12","CD4M",true);
        setSample("Pt13_CE03003_CD4M_Blood","narco","Pt13","CD4M",true);
        setSample("Pt14_C03004_CD4M_Blood","narco","Pt14","CD4M",true);
        setSample("Pt15_K15_CD4M_Blood","narco","Pt15","CD4M",true);
        setSample("Pt16_K16_CD4M_Blood","narco","Pt16","CD4M",true);
        setSample("Pt17_S04_CD4M_Blood","narco","Pt17","CD4M",true);
        setSample("Pt18_S06_CD4M_Blood","narco","Pt18","CD4M",true);
        setSample("Pt20_C01P05_CD4M_Blood","narco","Pt20","CD4M",true);
        setSample("Pt7_C01P03_CD4M_Blood","narco","Pt7","CD4M",true);
        setSample("S08_CD4_M_Blood","narco","Pt5","CD4M",true);
        setSample("Pt_22_CD4_M_PBMCs","narco","Pt22","CD4M",true);
        setSample("Pt_23_CD4_M_PBMCs","narco","Pt23","CD4M",true);
        setSample("Pt_24_CD4_M_PBMCs","narco","Pt24","CD4M",true);
        setSample("Pt_2_CD4_M_PBMCs","narco","Pt2","CD4M",true);

        //cd4 naive
        setSample("BA_CD4_N_Blood","narco","Pt1","CD4N",true);
        setSample("K08_CD4_N_Blood","narco","Pt8","CD4N",true);
        setSample("K09_CD4_N_Blood","narco","Pt4","CD4N",true);
        setSample("NCL7_cd4_naive_blood","narco","Pt10","CD4N",true);
        setSample("NarcoTCE_Naive-_PBMCs","narco","Pt2","CD4N",true);
        setSample("Pt11_K10_CD4N_Blood","narco","Pt11","CD4N",true);
        setSample("Pt12_K11_CD4N_Blood","narco","Pt12","CD4N",true);
        setSample("Pt13_CE03003_CD4N_Blood","narco","Pt13","CD4N",true);
        setSample("Pt14_C03004_CD4N_Blood","narco","Pt14","CD4N",true);
        setSample("Pt15_K15_CD4N_Blood","narco","Pt15","CD4N",true);
        setSample("Pt16_K16_CD4N_Blood","narco","Pt16","CD4N",true);
        setSample("Pt17_S04_CD4N_Blood","narco","Pt17","CD4N",true);
        setSample("Pt18_S06_CD4N_Blood","narco","Pt18","CD4N",true);
        setSample("Pt20_C01P05_CD4N_Blood","narco","Pt20","CD4N",true);
        setSample("Pt7_C01P03_CD4N_Blood","narco","Pt7","CD4N",true);
        setSample("S08_CD4_N_Blood","narco","Pt5","CD4N",true);
        setSample("Pt_22_CD4_N_PBMCs","narco","Pt22","CD4N",true);
        setSample("Pt_23_CD4_N_PBMCs","narco","Pt23","CD4N",true);
        setSample("Pt_24_CD4_N_PBMCs","narco","Pt24","CD4N",true);

        //cd8 mem
        setSample("K02_CD8_M_Blood","narco","Pt9","CD8M",true);
        setSample("NCL6_CD8_M_Blood","narco","Pt3","CD8M",true);
        setSample("Pt13_CE03003_CD8M_Blood","narco","Pt13","CD8M",true);
        setSample("Pt16_K16_CD8M_Blood","narco","Pt16","CD8M",true);
        setSample("Pt_10_CD8_M_PBMCs","narco","Pt10","CD8M",true);
        setSample("Pt_22_CD8_M_PBMCs","narco","Pt22","CD8M",true);
        setSample("Pt_23_CD8_M_PBMCs","narco","Pt23","CD8M",true);
        setSample("Pt_24_CD8_M_PBMCs","narco","Pt24","CD8M",true);
        setSample("Pt_2_CD8_M_PBMCs","narco","Pt2","CD8M",true);

        //csf
        setSample("Pt7_C01P03_CD4+CSF","narco","Pt7","CD4",false);
        setSample("Pt13_CE03003_CD8+CSF","narco","Pt13","CD8",false);
        setSample("Pt14_C03004_CD4+CSF","narco","Pt14","CD4",false);
        setSample("Pt14_C03004_CD8+CSF","narco","Pt14","CD8",false);
        setSample("Pt7_C01P03_CD8+CSF","narco","Pt7","CD8",false);
        setSample("Pt13_CE03003_CD4+CSF","narco","Pt13","CD4",false);
        setSample("NCL7_cd8_CSF","narco","Pt10","CD8",false);
        setSample("NCL7_cd4_CSF","narco","Pt10","CD4",false);
        setSample("NarcoTCE_CD4+CD8+_CSF","narco","Pt2","CD4CD8",false);
        setSample("NarcoTCE_CD4+_tot_CSF","narco","Pt2","CD4",false);
        setSample("NarcoTCE_CD8+_CSF","narco","Pt2","CD8",false);
        setSample("NarcoBA_CD3+CD4+_CSF","narco","Pt1","CD4",false);
        setSample("NarcoBA_CD3+CD8+_CSF","narco","Pt1","CD8",false);
        setSample("Pt_22_CD4+CSF","narco","Pt22","CD4",false);
        setSample("Pt_22_CD8+CSF","narco","Pt22","CD8",false);
        setSample("Pt_23_CD4+CSF","narco","Pt23","CD4",false);
        setSample("Pt_23_CD8+CSF","narco","Pt23","CD8",false);
        setSample("Pt_23_CD4+CD8+CSF","narco","Pt23","CD4CD8",false);
    }


    private void implementMs(){
        //cd4M
        setSample("MSG1_CD4M_Blood","ms","MS1","CD4M",true);
        setSample("MSG2_CD4M_Blood","ms","MS2","CD4M",true);
        setSample("MSG4_CD4M_Blood","ms","MS4","CD4M",true);
        setSample("Pt19_K22_CD4M_Blood","ms","Pt19","CD4M",true);

        //cd8M
        setSample("MSG2_CD8M_Blood","ms","MS2","CD8M",true);

        //cd4 naive
        setSample("MSG1_CD4N_Blood","ms","MS1","CD4N",true);
        setSample("MSG2_CD4N_Blood","ms","MS2","CD4N",true);
        setSample("MSG4_CD4N_Blood","ms","MS4","CD4N",true);
        setSample("Pt19_K22_CD4N_Blood","ms","Pt19","CD4N",true);

        //CSFexvivo
        setSample("MSG4_CXCR3-CFSE-MOG+MBP","ms","MS4","CXCR3-CFSE-",false);
        setSample("MSG1_CXCR3-CFSE-MOG+MBP","ms","MS1","CXCR3-CFSE-",false);
        setSample("MSG1_CD4M-CFSE-MOG+MBP","ms","MS1","CD4M-CFSE-",false);

    }

    private void implementHd(){
        //cd4M
        setSample("HD1_K23_CD4M_Blood","hd","HD1","CD4M",true);
        setSample("HD2_K24_CD4M_Blood","hd","HD2","CD4M",true);
        setSample("HD3_K20_CD4M_Blood","hd","HD3","CD4M",true);
        setSample("HD4_K21_CD4M_Blood","hd","HD4","CD4M",true);
        setSample("HD5_K25_CD4M_Blood","hd","HD5","CD4M",true);
        setSample("HD10_CD4_M_PBMCs","hd","HD10","CD4M",true);
        setSample("HD11_CD4_M_PBMCs","hd","HD11","CD4M",true);
        setSample("HD12_CD4_M_PBMCs","hd","HD12","CD4M",true);
        setSample("HD13_CD4_M_PBMCs","hd","HD13","CD4M",true);
        setSample("HD14_CD4_M_PBMCs","hd","HD14","CD4M",true);
        setSample("HD15_CD4_M_PBMCs","hd","HD15","CD4M",true);
        setSample("HD16_CD4_M_PBMCs","hd","HD16","CD4M",true);
        setSample("HD17_CD4_M_PBMCs","hd","HD17","CD4M",true);
        setSample("HD18_CD4_M_PBMCs","hd","HD18","CD4M",true);
        setSample("HD19_CD4_M_PBMCs","hd","HD19","CD4M",true);
        setSample("HD20_CD4_M_PBMCs","hd","HD20","CD4M",true);
        setSample("HD7_CD4_M_PBMCs","hd","HD7","CD4M",true);
        setSample("HD9_CD4_M_PBMCs","hd","HD9","CD4M",true);

        //cd4 naive
        setSample("HD1_K23_CD4N_Blood","hd","HD1","CD4N",true);
        setSample("HD2_K24_CD4N_Blood","hd","HD2","CD4N",true);
        setSample("HD3_K20_CD4N_Blood","hd","HD3","CD4N",true);
        setSample("HD4_K21_CD4N_Blood","hd","HD4","CD4N",true);
        setSample("HD5_K25_CD4N_Blood","hd","HD5","CD4N",true);
        setSample("HD10_CD4_N_PBMCs","hd","HD10","CD4N",true);
        setSample("HD11_CD4_N_PBMCs","hd","HD11","CD4N",true);
        setSample("HD12_CD4_N_PBMCs","hd","HD12","CD4N",true);
        setSample("HD13_CD4_N_PBMCs","hd","HD13","CD4N",true);
        setSample("HD14_CD4_N_PBMCs","hd","HD14","CD4N",true);
        setSample("HD15_CD4_N_PBMCs","hd","HD15","CD4N",true);
        setSample("HD16_CD4_N_PBMCs","hd","HD16","CD4N",true);
        setSample("HD17_CD4_N_PBMCs","hd","HD17","CD4N",true);
        setSample("HD18_CD4_N_PBMCs","hd","HD18","CD4N",true);
        setSample("HD19_CD4_N_PBMCs","hd","HD19","CD4N",true);
        setSample("HD20_CD4_N_PBMCs","hd","HD20","CD4N",true);
        setSample("HD7_CD4_N_PBMCs","hd","HD7","CD4N",true);
        setSample("HD9_CD4_N_PBMCs","hd","HD9","CD4N",true);

        //cd8M
        setSample("HD1_K23_CD8M_Blood","hd","HD1","CD8M",true);
        setSample("HD2_K24_CD8M_Blood","hd","HD2","CD8M",true);
        setSample("HD3_K20_CD8M_Blood","hd","HD3","CD8M",true);
        setSample("HD4_K21_CD8M_Blood","hd","HD4","CD8M",true);
        setSample("HD5_K25_CD8M_Blood","hd","HD5","CD8M",true);
        setSample("HD11_CD8_M_PBMCs","hd","HD11","CD8M",true);
        setSample("HD12_CD8_M_PBMCs","hd","HD12","CD8M",true);
        setSample("HD13_CD8_M_PBMCs","hd","HD13","CD8M",true);
        setSample("HD14_CD8_M_PBMCs","hd","HD14","CD8M",true);
        setSample("HD15_CD8_M_PBMCs","hd","HD15","CD8M",true);
        setSample("HD16_CD8_M_PBMCs","hd","HD16","CD8M",true);
        setSample("HD17_CD8_M_PBMCs","hd","HD17","CD8M",true);
        setSample("HD18_CD8_M_PBMCs","hd","HD18","CD8M",true);
        setSample("HD19_CD8_M_PBMCs","hd","HD19","CD8M",true);
        setSample("HD20_CD8_M_PBMCs","hd","HD20","CD8M",true);
        setSample("HD7_CD8_M_PBMCs","hd","HD7","CD8M",true);
        setSample("HD9_CD8_M_PBMCs","hd","HD9","CD8M",true);

    }

    private void implementHd6(){
        //cd4M
        setSample("HD6_PC_CD4M_Blood","hd6","HD6","CD4M",true);
        //cd4naive
        setSample("HD6_PC_CD4N_Blood","hd6","HD6","CD4N",true);
        //cd8M
        setSample("HD6_PC_CD8M_Blood","hd6","HD6","CD8M",true);
        //CSF
        setSample("HD6_PC_CD4+CSF","hd6","HD6","CD4",false);
        setSample("HD6_PC_CD8+CSF","hd6","HD6","CD8",false);
    }

    private void setSample(String sampleId,String categ,String donor,String subType,boolean isBlood){
        Sample sample = new Sample();
        sample.setId(sampleId);
        sample.setCateg(categ);
        sample.setDonor(donor);
        sample.setSubtype(subType);
        sample.setBlood(isBlood);
        sampleIdToSample.put(sampleId,sample);
    }

    public HashMap<String, Sample> getSampleIdToSample() {
        return sampleIdToSample;
    }
}

package ch.irb.narcolepsy;

import ch.irb.utils.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

/**
 Copyright 2018 - Mathilde Foglierini Perez
 This code is distributed open source under the terms of the GNU Free Documentation License.

 This is the main class used to search for antigen specific clonotypes in all donors' repertoires.
 It needs 2 input file in a folder:
  * AgSpecificClonotypes.txt, a table containing the antigen specific clonotypes annotated by IMGT (see table for the format)
  * RearrangementDetails.tsv, a table regrouping all clonotypes from all donors' repertoire, generated automatically
    by ImmunoSEQ Analyzer from the Rearrangement View then Export view to file (default parameters)
  It will create an output file 'AgSpecificClonotypes_found.tsv' that list all antigen specific clonotypes and the related
  sample/donor it was found in.

  A clonotype is defined as a unique combination of a CDR3 amino acid sequence and its related V gene.
 */

public class FindAgSpecificTcrs {
    private String anlysisFolder = "test";
    private HashMap<String, Sample> sampleIdToSample;
    private File folder = new File("C:\\Users\\mperez\\Documents\\" + anlysisFolder );

    private HashMap<String, LinkedHashSet<String>> agSpeTcrToCompleteTCRs = new HashMap<>();
    private LinkedHashMap<String, String> tcrKeyToLine = new LinkedHashMap<>();
    private String header;
    private LinkedHashSet<String> selectedTCRs = new LinkedHashSet<>();
    private HashMap<String, ArrayList<String>> tcrToSamples = new HashMap<>();
    private ArrayList<String> selectedSamples = new ArrayList<>();

    public static void main(String[] args) {
        FindAgSpecificTcrs analyzeAgSpecificTcrs = new FindAgSpecificTcrs();
    }

    public FindAgSpecificTcrs() {
        try {
            //we set all the names sample and related donor
            ImplementSamples implementSamples = new ImplementSamples("all");
            sampleIdToSample = implementSamples.getSampleIdToSample();

            //Parse the AgSpeTcr file
            File agSpeFile =new File(folder.getPath()+Consts.fs+"AgSpecificClonotypes.txt");
            AgSpecificTcrParser agSpecificTcrParser = new AgSpecificTcrParser(agSpeFile);
            tcrKeyToLine = agSpecificTcrParser.getTcrKeyToLines();
            System.out.println("Number of KEYS " + tcrKeyToLine.size());
            header = agSpecificTcrParser.getHeader();

            File dbFile = new File(folder.getPath()+Consts.fs+"RearrangementDetails.tsv");
            SearchForIdenticalTCR searchForIdenticalTCR = new SearchForIdenticalTCR(dbFile, tcrKeyToLine);
            agSpeTcrToCompleteTCRs = searchForIdenticalTCR.getAgSpeTcrToCompleteTCRs();
            selectedTCRs = searchForIdenticalTCR.getSelectedTCRs();

            //get the samples list for each Ag spe Tcr
            parseTsvToStoreRelatedSamples(dbFile);

            //outputs
            writeOutput1();
            System.out.println("Number of selected samples: " + selectedSamples.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
    This method parses the RearrangementDetails.tsv file and will store the sample Id of each clonotype that was tagged
    as an Ag specific clonotype (results from SearchForIdenticalTCR class)
     */
    private void parseTsvToStoreRelatedSamples(File file) throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(file));
        int lineIndex = 0;
        String line = "";
        while ((line = fileReader.readLine()) != null) {
            String[] cells = line.split("\t");
            if (lineIndex > 0) {
                String tcr = cells[3] + "_" + cells[5];
                //special case
                if (cells[5].contains("-or")){
                    tcr = cells[3]+"_"+cells[5].split("_")[0];
                }
                // String tcr = cells[0] + "_" + cells[1];
                if (selectedTCRs.contains(tcr)) { //we process it
                    ArrayList<String> samplesList = new ArrayList<>();
                    if (tcrToSamples.containsKey(tcr)) {
                        samplesList = tcrToSamples.get(tcr);
                    }
                    samplesList.add(cells[0]);
                    tcrToSamples.put(tcr, samplesList);
                }
            }
            lineIndex++;
        }
        fileReader.close();
        System.out.println("Number of clonotypes " + lineIndex + " for " + file.getName());
    }

    /*
    This method will create an output file like the following: each line correspond to an antigen specific clonotype
    (same input line that 'AgSpecificClonotypes.txt' file) and will add extra columns if the clonotype was found in
    the repertoire of other donors (name of donors, number of donors, if the V gene is identical etc...)
     */
    private void writeOutput1() throws IOException {
        File outFile = new File(folder.getPath() + Consts.fs + "AgSpecificClonotypes_found.tsv");
        BufferedWriter out = new BufferedWriter(new FileWriter(outFile));
        out.write(header + "\tFound in\tClonotype\tDiff\tSameVgene\tDonors number\tDonors" + Consts.ls);
        for (String tcrKey : tcrKeyToLine.keySet()) {
            //System.out.println("key "+tcrKey);
            out.write(tcrKeyToLine.get(tcrKey));

            if (agSpeTcrToCompleteTCRs.containsKey(tcrKey)) {
                LinkedHashSet<String> keysFound = agSpeTcrToCompleteTCRs.get(tcrKey);
                int in=0;
                for (String relatedTcr :keysFound){

                    if (in>0){ //rwirte Ag spe tcr
                        out.write(Consts.ls+tcrKeyToLine.get(tcrKey));
                    }
                    //String relatedTcr = agSpeTcrToCompleteTCRs.get(tcrKey);
                    //System.out.print(" WITH "+relatedTcr);
                    ArrayList<String> donors = new ArrayList<>();
                    out.write("\t" + tcrToSamples.get(relatedTcr));
                    //check if there is 1 or 0 diff in the CDR3 aa seq
                    String cdr3 = tcrKey.split("_")[0];
                    String relatedCdr3 = relatedTcr.split("_")[0];
                    int i = 0;
                    int diff = 0;
                    for (char aa : cdr3.toCharArray()) {
                        if (aa != relatedCdr3.charAt(i)) {
                            diff++;
                        }
                        i++;
                    }
                    boolean samevGene= isSameVGene(relatedTcr.split("_")[1],tcrKey.split("_")[1]);
                    out.write("\t" + relatedTcr + "\t" + diff+ "\t" +samevGene);
                    //System.out.println("related TCR "+relatedTcr);
                    for (String samp : tcrToSamples.get(relatedTcr)) {
                        //we add this sample to the general list
                        if (!selectedSamples.contains(samp)) {
                            selectedSamples.add(samp);
                        }
                        Sample sample = sampleIdToSample.get(samp);
                        //out.write(samp+" ");
                        String donor = sample.getDonor();
                        if (!donors.contains(donor)) {
                            donors.add(donor);
                        }
                    }
                    out.write("\t" + donors.size() + "\t" + donors);
                    in++;
                }
            }
            out.write(Consts.ls);
        }
        out.close();
    }


    /*
    This method checks if the v gene from a clonotype (ImmunoSeq nomenclature) is the same than the v Gene of an antigen
    specific clonotype (IMGT nomenclature)
     */
    private boolean isSameVGene (String vGene, String imgtVgene){
        if (vGene.contains("*")){
            vGene = vGene.split("\\*")[0];
        }
        if (imgtVgene.contains(", or ")) {
            String[] spli = imgtVgene.split(", or ");
            for (String vGe : spli) {
                //System.out.println("OR- "+tcrVgene);
                IMGTnomenclatureConverter conv = new IMGTnomenclatureConverter(vGe.split("\\*")[0], true);
                String immunoSeqVGene = conv.getImmunoSeqGene();
                //special case where an ImmunoSeq v Gene is a vFamily, we check the v Family directly
                if (!vGene.contains("-")){
                    if (conv.getImmunoSeqFamily().equals(vGene)){
                        return true;
                    }
                }
                if (immunoSeqVGene.equals(vGene)) {
                    //System.out.println("SAME v GENE");
                    return true;
                }
            }
        } else {
            // System.out.println(tcrVgene);
            IMGTnomenclatureConverter conv = new IMGTnomenclatureConverter(imgtVgene.split("\\*")[0], true);
            String immunoSeqVGene = conv.getImmunoSeqGene();
            //special case where an ImmunoSeq v Gene is a vFamily, we check the v Family directly
            if (!vGene.contains("-")){
                if (conv.getImmunoSeqFamily().equals(vGene)){
                    return true;
                }
            }
            if (immunoSeqVGene.equals(vGene)) {
                return true;
            }
        }
        return false;
    }

}

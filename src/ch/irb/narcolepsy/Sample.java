package ch.irb.narcolepsy;

import java.util.ArrayList;

/**
 Copyright 2018 - Mathilde Foglierini Perez
 This code is distributed open source under the terms of the GNU Free Documentation License.

 Sample object.
 */
public class Sample {

    private String id;
    private String donor;
    private ArrayList <TcrObject> tcrObjLists = new ArrayList<>();
    private boolean isBlood;
    private String subtype; //can ne naive mem cd4 or cd8
    private String categ;//can be narco hd or ms

    public Sample(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<TcrObject> getTcrObjLists() {
        return tcrObjLists;
    }

    public void addTcrObj(TcrObject tcrObj) {
        this.tcrObjLists.add(tcrObj);
    }

    public String getDonor() {
        return donor;
    }

    public void setDonor(String donor) {
        this.donor = donor;
    }


    public boolean isBlood() {
        return isBlood;
    }

    public void setBlood(boolean blood) {
        isBlood = blood;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getCateg() {
        return categ;
    }

    public void setCateg(String categ) {
        this.categ = categ;
    }

}

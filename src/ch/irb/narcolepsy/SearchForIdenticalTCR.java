package ch.irb.narcolepsy;

import ch.irb.utils.IMGTnomenclatureConverter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;

/**
  Copyright 2018 - Mathilde Foglierini Perez
 This code is distributed open source under the terms of the GNU Free Documentation License.

 * This class works with a rearrangement input file (from ImmunoSeq) and a list of key cdr3aa_vGene to look for.
 * It will store in a hashset the clonotypes found in other donors' repertoires (same V gene and same V family
   at this point, later we will specify if they have the same v gene).
 */
public class SearchForIdenticalTCR {
    private HashMap<String, LinkedHashSet<String>> agSpeTcrToCompleteTCRs = new HashMap<>();
    private HashMap<String, ArrayList<String>> agSpeTcrToOnlyCdr3TCRs = new HashMap<>();
    private LinkedHashSet<String> selectedTCRs = new LinkedHashSet<>(); //TCRs that matches with Ag spe ones
    private int mismatches = 0; //max mismatches

    public SearchForIdenticalTCR(File inputFile, HashMap<String, String> tcrKeyToLine) throws IOException {
        //1. we parse the input file and look if it matches with the given list
        HashMap<Integer, LinkedHashSet<String>> cdr3LengthToTcrs = new HashMap<>();
        BufferedReader fileReader = new BufferedReader(new FileReader(inputFile));
        String line = "";
        int index = 0;
        while ((line = fileReader.readLine()) != null) {
            if (index > 0) {
                String[] cells = line.split("\t");
                String cdr3 = cells[3];
                String key = cdr3 + "_" + cells[5];
                LinkedHashSet<String> keys = cdr3LengthToTcrs.get(cdr3.length());
                if (keys == null) {
                    keys = new LinkedHashSet<String>();
                }
                keys.add(key);
                cdr3LengthToTcrs.put(cdr3.length(), keys);
            }
            index++;
        }
        fileReader.close();
        System.out.println("Number of different CDR3 length " + cdr3LengthToTcrs.size());

        //we look for the Ag spe
        int counter = tcrKeyToLine.size();
        for (String key : tcrKeyToLine.keySet()) {
            counter--;
            System.out.println(counter + " -> Looking for " + key);
            String tcrCdr3 = key.split("_")[0];
            String tcrVgene = key.split("_")[1];
            LinkedHashSet<String> allTcrs = cdr3LengthToTcrs.get(tcrCdr3.length());
            System.out.println("... in " + allTcrs.size() + " Tcrs");
            for (String allTcr : allTcrs) {
                String cdr3 = allTcr.split("_")[0];
                String vGene = allTcr.split("_")[1];
                if (isIdentical(tcrCdr3, tcrVgene, cdr3, vGene)) {
                    //System.out.println("IDENTICAL "+tcrCdr3+" and "+cdr3);
                    String keyFound = cdr3 + "_" + vGene;
                    selectedTCRs.add(keyFound);
                    LinkedHashSet<String> keys = new LinkedHashSet<>();
                    if (agSpeTcrToCompleteTCRs.containsKey(key)) {
                        keys = agSpeTcrToCompleteTCRs.get(key);
                    }
                    keys.add(keyFound);
                    agSpeTcrToCompleteTCRs.put(key, keys);
                }
            }
        }

    }

    private boolean isIdentical(String tcrCdr3, String tcrVgene, String cdr3, String vGene) {
        int diff = 0;
        int i = 0;
        for (char aa : cdr3.toCharArray()) {
            if (aa != tcrCdr3.charAt(i)) {
                diff++;
            }
            i++;
        }

        //we go out if mismatches > max mismatches (0 here)
        if (diff > mismatches) {
            return false;
        }

        boolean sameVfam = false;

        String vFam = vGene;
        if (vGene.contains("-")) {
            vFam = vGene.split("-")[0];
        }

        if (tcrVgene.contains(", or ")) {
            String[] spli = tcrVgene.split(", or ");
            for (String vGe : spli) {
                //System.out.println("OR- "+tcrVgene);
                IMGTnomenclatureConverter conv = new IMGTnomenclatureConverter(vGe.split("\\*")[0], true);
                //String immunoSeqVGene = conv.getImmunoSeqGene();
                String immunoSeqVfam = conv.getImmunoSeqFamily();
                if (immunoSeqVfam.equals(vFam)) {
                    sameVfam = true;
                    break;
                }
            }
        } else {
            // System.out.println(tcrVgene);
            IMGTnomenclatureConverter conv = new IMGTnomenclatureConverter(tcrVgene.split("\\*")[0], true);
            String immunoSeqVfam = conv.getImmunoSeqFamily();
            if (immunoSeqVfam.equals(vFam)) {
                sameVfam = true;
            }
        }


        if (sameVfam) {
            return true;
        } else {
            return false;
        }
    }

    public HashMap<String, LinkedHashSet<String>> getAgSpeTcrToCompleteTCRs() {
        return agSpeTcrToCompleteTCRs;
    }

    public HashMap<String, ArrayList<String>> getAgSpeTcrToOnlyCdr3TCRs() {
        return agSpeTcrToOnlyCdr3TCRs;
    }

    public LinkedHashSet<String> getSelectedTCRs() {
        return selectedTCRs;
    }
}

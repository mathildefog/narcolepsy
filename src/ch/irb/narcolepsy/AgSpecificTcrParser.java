package ch.irb.narcolepsy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 Copyright 2018 - Mathilde Foglierini Perez
 This code is distributed open source under the terms of the GNU Free Documentation License.

  This class takes in input a txt file containing the info related to the Ag specific clonotypes, that were
  annotated using IMGT. See input file: AgSpecificClonotypes.txt in input directory.
 */
public class AgSpecificTcrParser {

    private File agSpeFile ;
    private LinkedHashMap<String, String> tcrKeyToLine = new LinkedHashMap<>();
    private LinkedHashMap<String, String> cdr3ToMainInfo = new LinkedHashMap<>();
    private HashMap<String, String> tcrKeyToAg = new LinkedHashMap<>();
    private String header;

    public AgSpecificTcrParser(File file) throws IOException {
        this.agSpeFile =  file;
        BufferedReader fileReader = new BufferedReader(new FileReader(agSpeFile));
        int lineIndex = 0;
        String line = "";
        HashMap<String, Integer> headerToIndex = new HashMap<>();
        while ((line = fileReader.readLine()) != null) {
            if (lineIndex > 0) {
                String[] cells = line.split("\t");
                String tcrKey = cells[1].trim() + "_" + cells[2].trim();
                if (tcrKeyToLine.containsKey(tcrKey)){
                    System.out.println("Be careful, tcr already exists ! "+tcrKey);
                }
                tcrKeyToLine.put(tcrKey, line);
                if (cdr3ToMainInfo.containsKey(cells[1].trim())){
                    System.out.println("Be careful, CDR3 already exists ! "+cells[1].trim());
                }
                // mainInfo = cloneId/CDR3/Vgene/Jgene/Pt/Ag
                String mainInfo=cells[0].trim()+"\t"+cells[1].trim()+"\t"+cells[2].trim().replaceAll("Homsap ","")
                        +"\t"+cells[3].trim().replaceAll("Homsap ","")+"\t"+cells[5].trim()+"\t"+cells[6].trim();
                cdr3ToMainInfo.put(cells[1].trim(),mainInfo);
                tcrKeyToAg.put(tcrKey,cells[6]);
            } else {
                header = line;
            }
            lineIndex++;
        }
        fileReader.close();
    }

    public LinkedHashMap<String, String> getTcrKeyToLines() {
        return tcrKeyToLine;
    }

    public LinkedHashMap<String, String> getCdr3ToMainInfo() {
        return cdr3ToMainInfo;
    }

    public HashMap<String, String> getTcrKeyToAg() {
        return tcrKeyToAg;
    }

    public String getHeader() {
        return header;
    }
}

package ch.irb.narcolepsy;

import java.util.HashMap;

/**
 Copyright 2018 - Mathilde Foglierini Perez
 This code is distributed open source under the terms of the GNU Free Documentation License.

 * Tcr class which stores the tcr data related to ONE key (key = cdr3+vgene)
 * It stores the frequencies for each sample Id, the number of donors, categ that share this tcr
 */
public class TcrObject implements Comparable {

    private boolean isPublic = false; //is public or not for all donors AND categories
    private boolean isExclusivelyPublic = false; // is public only in 1 categ, between 3 donors (and not present in  another categ)
    private boolean isPublicInBoth=false;
    private HashMap<String,Boolean>categToIsPublic = new HashMap<>();
    private String key; // CDR3 + Vgene
    private int donorsNumberSharingThisTcr;
    private int categoriesNumberSharingThisTcr;
    private HashMap<String, Double> sampleToFreq = new HashMap<>();
    private HashMap<String, Integer> sampleToRank = new HashMap<>();

    public TcrObject() { }

    public boolean isExclusivelyPublic() {
        return isExclusivelyPublic;
    }

    public void setExclusivelyPublic(boolean exclusivelyPublic) {
        isExclusivelyPublic = exclusivelyPublic;
    }

    public int getDonorsNumberSharingThisTcr() {
        return donorsNumberSharingThisTcr;
    }

    public void setDonorsNumberSharingThisTcr(int donorsNumberSharingThisTcr) {
        this.donorsNumberSharingThisTcr = donorsNumberSharingThisTcr;
    }

    public boolean isPublicInBoth() {
        return isPublicInBoth;
    }

    public void setPublicInBoth(boolean publicInBoth) {
        isPublicInBoth = publicInBoth;
    }

    public HashMap<String, Boolean> getCategToIsPublic() {
        return categToIsPublic;
    }

    public void addCategToIsPublic(String categ, Boolean isPublic) {
        this.categToIsPublic.put(categ, isPublic);
    }

    public HashMap<String, Double> getSampleToFreq() {
        return sampleToFreq;
    }

    public void setSampleToFreq(HashMap<String, Double> sampleToFreq) {
        this.sampleToFreq = sampleToFreq;
    }

    public HashMap<String, Integer> getSampleToRank() {
        return sampleToRank;
    }

    public void setSampleToRank(HashMap<String, Integer> sampleToRank) {
        this.sampleToRank = sampleToRank;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public int getCategoriessNumberSharingThisTcr() {
        return categoriesNumberSharingThisTcr;
    }

    public void setCategoriessNumberSharingThisTcr(int categoriessNumberSharingThisTcr) {
        this.categoriesNumberSharingThisTcr = categoriessNumberSharingThisTcr;
    }

    public int getDonorsNumberForCategory(String category,HashMap<String,Sample> sampleIdToSample){
        int sharedDonors=0;
        for (String sampleId: sampleToFreq.keySet()){
            Sample sample = sampleIdToSample.get(sampleId);
            if (sample.getCateg().equals(category) && sample.isBlood()){
                sharedDonors++;
            }
        }
        return sharedDonors;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}

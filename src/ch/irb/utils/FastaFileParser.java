package ch.irb.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 Copyright 2018 - Mathilde Foglierini Perez
 This code is distributed open source under the terms of the GNU Free Documentation License.

 FastaParser class, need fasta file in input and return a HashMap idToSeq
 */
public class FastaFileParser {

	private HashMap<String, String> fastaIdToSequence = new HashMap<>();
	private LinkedHashMap<String, String> sameOrderFastaIdToSequence = new LinkedHashMap<>();
	private File file = null;

	public static void main(String[] args) {
        try {
            FastaFileParser parser = new FastaFileParser(new File("D:\\Users\\Mathilde\\Documents\\KATHRIN\\" +
                    "SwitchRegionAnalysis\\MinION\\10thRun_16_07_17\\100reads_mini4500bp.fasta"));
            System.out.println("Number of fasta entries "+parser.getFastaIdToSequence().size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	public FastaFileParser(File file) throws IOException {
		this.file = file;
		parseFile();
	}

	private void parseFile() throws IOException {
		BufferedReader fileReader = new BufferedReader(new FileReader(file.getPath()));
		String line = "";
		String fastaId=null;
		String sequence="";
		while ((line = fileReader.readLine()) != null) {
			if (line.matches(">.*")){ //new entry
				//we record the previous entry
				if (fastaId!=null && sequence.length()>0){
					String seq = sequence.toUpperCase().trim();
					fastaIdToSequence.put(fastaId, seq);
					sameOrderFastaIdToSequence.put(fastaId, seq);
				}
				fastaId= line.replace(">","").trim();
				//System.out.println(fastaId);
				sequence="";
			}
			else {
				sequence+= line.trim();
			}
		}

		//we store the last entry
		if (fastaId!=null && sequence.length()>0){
			String seq = sequence.toUpperCase().trim();
			fastaIdToSequence.put(fastaId, seq);
			sameOrderFastaIdToSequence.put(fastaId, seq);
		}
	}

	
	public HashMap<String, String> getFastaIdToSequence(){
		return fastaIdToSequence;
	}
	
	public LinkedHashMap<String, String> getSameOrderFastaIdToSequence(){
		return sameOrderFastaIdToSequence;
	}
	
	public HashMap<String, ArrayList<String>> getSeqToFastaIds(){
		HashMap<String, ArrayList<String>> seqToIds = new HashMap<>();;
		for (String id: fastaIdToSequence.keySet()){
			String seq = fastaIdToSequence.get(id);
			ArrayList<String> ids = new ArrayList<>();
			if (seqToIds.containsKey(seq)){
				ids = seqToIds.get(seq);
			}
			ids.add(id);
			seqToIds.put(seq, ids);
		}
		return seqToIds;
	}
	
}

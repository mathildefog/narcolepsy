package ch.irb.utils;

import java.io.File;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 Copyright 2018 - Mathilde Foglierini Perez
 This code is distributed open source under the terms of the GNU Free Documentation License.

 Utils class.
 */

public final class Consts {

	public static final String ls = System.getProperty("line.separator");
	public static final String fs = System.getProperty("file.separator");
	public static final String[] aminoAcidsList = { "A", "G", "K", "N", "S", "D", "V", "L", "T", "I", "E", "Q",
			"R", "H", "M", "P", "C", "F", "W", "Y", "X", "-" };
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static final ArrayList<String> aminoAcids = new ArrayList(Arrays.asList(aminoAcidsList));

	public static final String[] immunoSeqVGeneList ={"TCRBV01-01","TCRBV02-01","TCRBV03","TCRBV04","TCRBV04-01","TCRBV04-02"
	,"TCRBV04-03","TCRBV04-04","TCRBV05","TCRBV05-01","TCRBV05-02","TCRBV05-03","TCRBV05-04","TCRBV05-05","TCRBV05-06"
			,"TCRBV05-07","TCRBV05-08","TCRBV06","TCRBV06-01","TCRBV06-04","TCRBV06-05","TCRBV06-06","TCRBV06-07","TCRBV06-08"
			,"TCRBV06-09","TCRBV07","TCRBV07-01","TCRBV07-02","TCRBV07-03","TCRBV07-04","TCRBV07-05","TCRBV07-06","TCRBV07-07"
			,"TCRBV07-08","TCRBV07-09","TCRBV08-02","TCRBV09-01","TCRBV10-01","TCRBV10-02","TCRBV10-03","TCRBV11","TCRBV11-01"
			,"TCRBV11-02","TCRBV11-03","TCRBV12","TCRBV12-01","TCRBV12-02","TCRBV12-05","TCRBV13-01","TCRBV14-01","TCRBV15-01"
			,"TCRBV16-01","TCRBV18-01","TCRBV19-01","TCRBV20","TCRBV20-01","TCRBV21-01","TCRBV22-01","TCRBV23-01","TCRBV24"
			,"TCRBV25","TCRBV25-01","TCRBV27-01","TCRBV28-01","TCRBV29-01","TCRBV30-01"	};
	public static final ArrayList<String> immunoSeqVGenes = new ArrayList(Arrays.asList(immunoSeqVGeneList));

	@SuppressWarnings("rawtypes")
	public static <K extends Comparable, V extends Comparable> Map<K, V> sortByValuesAsc(Map<K, V> map) {
		List<Entry<K, V>> entries = new LinkedList<Entry<K, V>>(map.entrySet());

		Collections.sort(entries, new Comparator<Entry<K, V>>() {

			@SuppressWarnings("unchecked")
			public int compare(Entry<K, V> o1, Entry<K, V> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		});

		// LinkedHashMap will keep the keys in the order they are inserted
		// which is currently sorted on natural ordering
		Map<K, V> sortedMap = new LinkedHashMap<K, V>();

		for (Entry<K, V> entry : entries) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

	@SuppressWarnings("rawtypes")
	public static <K extends Comparable, V extends Comparable> Map<K, V> sortByValuesDesc(Map<K, V> map) {
		List<Entry<K, V>> entries = new LinkedList<Entry<K, V>>(map.entrySet());

		Collections.sort(entries, new Comparator<Entry<K, V>>() {

			@SuppressWarnings("unchecked")
			public int compare(Entry<K, V> o1, Entry<K, V> o2) {
				return o2.getValue().compareTo(o1.getValue());
			}
		});

		// LinkedHashMap will keep the keys in the order they are inserted
		// which is currently sorted on natural ordering
		Map<K, V> sortedMap = new LinkedHashMap<K, V>();

		for (Entry<K, V> entry : entries) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}
	
    /*
     * Get the extension of a file.
     */  
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }

}
